#pragma once
#include "Segment3D.h"
#include "Vector3D.h"
#include <iostream>
#include <cmath>
#include <exception>

class MyException : public std::exception//����� ���������
{
public:
	MyException(const char* msg) :exception(msg) {}
};

Vector3D* Interse�t(Segment3D segment1, Segment3D segment2);

int main() {
	try {
		//���������� �����
		double x1 = -50, y1 = -50, z1 = 10;
		double x2 = 50, y2 = 50, z2 = 10;
		double x3 = -50, y3 = 50, z3 = 10;
		double x4 = 50, y4 = -50, z4 = 10;
		Segment3D AB(new Vector3D(x1, y1, z1), new Vector3D(x2, y2, z2));
		Segment3D CD(new Vector3D(x3, y3, z3), new Vector3D(x4, y4, z4));
		Vector3D* point = Interse�t(AB, CD);
		if (point != nullptr) {
			printf("M = {%lf, %lf, %lf}", point->GetX(), point->GetY(), point->GetZ());
		}
		else std::cout << "Segments don't intersect";
	}
	catch (const MyException& ex)
	{
		std::cout << ex.what();
		return 0;
	}
}

Vector3D* Interse�t(Segment3D segment1, Segment3D segment2) {
	Vector3D* point = nullptr;
	double eps = 10e-10;
	//����� � �������, ������� �� ������
	double x01 = segment1.GetStart()->GetX(), x02 = segment2.GetStart()->GetX();
	double vector11 = segment1.GetEnd()->GetX() - x01, vector12 = segment2.GetEnd()->GetX() - x02;
	double y01 = segment1.GetStart()->GetY(), y02 = segment2.GetStart()->GetY();
	double vector21 = segment1.GetEnd()->GetY() - y01, vector22 = segment2.GetEnd()->GetY() - y02;
	double z01 = segment1.GetStart()->GetZ(), z02 = segment2.GetStart()->GetZ();
	double vector31 = segment1.GetEnd()->GetZ() - z01, vector32 = segment2.GetEnd()->GetZ() - z02;
	//������� ����������� ���� ��������� ���������� s � t �� ��������� ������ � ��������������� ����
	double del = vector21 * vector12 / vector11 - vector22;	//�������� ���������, ���� ����� ����, �� ������ ������������
	if (del >= eps && vector11 >= 0) {
		double s = -(y01 - y02 - (x01 - x02) / vector11) / (vector21 * vector12 / vector11 - vector22);
		double t = (x01 - vector12 * s - x02) / -vector11;
		double x = vector12 * s + x02;
		double y = vector22 * s + y02;
		double z = vector32 * s + z02;
		if (abs(x - (vector11 * t + x01)) < eps && abs(y - (vector21 * t + y01)) < eps && abs(z - (vector31 * t + z01) < eps)	//���� ������� ������ �����������, �� ������ �������������� 
				&& segment1.InSegment(x, y, z) && segment2.InSegment(x, y, z)) {	//��������, ����� ����� ������ �� ����� ��������
			point = new Vector3D(x, y, z);
		}
	}
	return point;
}
